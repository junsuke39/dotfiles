## Settings
Pry.config.color = true
Pry.config.editor = "vim"
Pry.config.pager = false
#Pry.config.command_prefix = "%"
#Pry.config.auto_indent = false
#Pry.config.correct_indent = false
#Pry.config.history.file = "~/.irb_history"
#Pry.config.prompt = proc { |obj, nest_level, _| "#{obj}:#{nest_level}> " }
#Pry.config.prompt_name = File.basename(Dir.pwd)

Pry.config.prompt = proc do |obj, level, _|
  prompt = ""
  prompt << "#{Rails.version}@" if defined?(Rails)
  prompt << "#{RUBY_VERSION}"
  "#{prompt} (#{obj})> "
end

## Alias
Pry.commands.alias_command 'pwd', 'nesting'
Pry.commands.alias_command 'w', 'whereami'

## Default require
%w[rubygems pp logger].each do |gem|
  begin
    require gem
  rescue LoadError
  end
end

## Default Command Set, add custom methods here:
default_command_set = Pry::CommandSet.new do
  command "copy", "Copy argument to the clip-board" do |str|
     IO.popen('pbcopy', 'w') { |f| f << str.to_s }
  end
  command "cl" do
    system 'clear'
    if ENV['RAILS_ENV']
      output.puts "Rails Environment: " + ENV['RAILS_ENV']
    end
  end
  command "sql", "Send sql over AR." do |query|
    if ENV['RAILS_ENV'] || defined?(Rails)
      pp ActiveRecord::Base.connection.select_all(query)
    else
      pp "Pry did not require the environment, try `pconsole`"
    end
  end
  command "caller_method" do |depth|
    depth = depth.to_i || 1
    if /^(.+?):(\d+)(?::in `(.*)')?/ =~ caller(depth+1).first
      file   = Regexp.last_match[1]
      line   = Regexp.last_match[2].to_i
      method = Regexp.last_match[3]
      output.puts [file, line, method]
    end
  end
end
Pry.config.commands.import default_command_set
#Pry.config.should_load_plugins = false

## AwesomePrint
# refs: https://github.com/pry/pry/wiki/FAQ#wiki-awesome_print
if defined? AwesomePrint
  begin
    require 'awesome_print'
    Pry.config.print = proc { |output, value| Pry::Helpers::BaseHelpers.stagger_output("=> #{value.ai}", output) }
    # Pry.config.print = proc { |output, value| output.puts value.ai } #ページングなし
  rescue LoadError => err
    puts "no awesome_print :("
    puts err
  end
end

## Hirb
# refs: https://github.com/pry/pry/wiki/FAQ#wiki-hirb
if defined? Hirb
 Hirb::View.instance_eval do
   def enable_output_method
     @output_method = true
     @old_print = Pry.config.print
     Pry.config.print = proc do |output, value|
       Hirb::View.view_or_page_output(value) || @old_print.call(output, value)
     end
   end
   def disable_output_method
     Pry.config.print = @old_print
     @output_method = nil
   end
 end
 Hirb.enable
end

# refs: https://github.com/nixme/pry-debugger#tips
if defined?(PryDebugger) || defined?(PryByebug)
  Pry.commands.alias_command 'c', 'continue'
  Pry.commands.alias_command 's', 'step'
  Pry.commands.alias_command 'n', 'next'
  Pry.commands.alias_command 'f', 'finish'
end

