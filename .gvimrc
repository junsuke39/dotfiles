"フォント
set guifont=Ricty:h14
"colorschema
"colorscheme darkblue
"colorscheme macvim
colorscheme jellybeans
"ツールバーを消す
set guioptions-=T
"横スクロールを出す
set guioptions+=b
"ウィンドウの幅
set columns=120
"ウィンドウの高さ
set lines=60

