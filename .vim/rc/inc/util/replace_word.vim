"""カーソル位置の単語とヤンクした文字列を置換する "http://d.hatena.ne.jp/fuenor/20090402/1238678759
nnoremap <silent> cip ciw<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
nnoremap <silent> cp   ce<C-r>0<ESC>:let@/=@1<CR>:noh<CR>
vnoremap <silent> cp   c<C-r>0<ESC>:let@/=@1<CR>:noh<CR>

