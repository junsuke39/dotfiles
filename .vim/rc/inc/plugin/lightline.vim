"https://github.com/itchyny/lightline.vim
"http://leafcage.hateblo.jp/entry/2013/10/21/lightlinevim-customize

NeoBundle 'itchyny/lightline.vim'

let g:lightline = {}
let g:lightline.colorscheme = 'wombat'
let g:lightline.component = {}
let g:lightline.component.rows = '%L'
let g:lightline.component.cd = '%.35(%{fnamemodify(getcwd(), ":~")}%)'
let g:lightline.active = {}
let g:lightline.active.left = [['mode', 'modified', 'readonly', 'paste'], ['relativepath']]
let g:lightline.active.right = [['lineinfo'], ['rows'], ['fileencoding', 'fileformat', 'filetype']]
let g:lightline.inactive = {}
let g:lightline.inactive.left = g:lightline.active.left
let g:lightline.inactive.right = g:lightline.active.right
