"""zencoding-vim
NeoBundle 'mattn/zencoding-vim'
let g:user_zen_settings = { 'lang':'ja', 'indentation':'  ' }
let g:user_zen_expandabbr_key = '<D-e>'
imap <D-e> <C-y>,

