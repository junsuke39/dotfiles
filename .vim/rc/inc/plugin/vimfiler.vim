"vimfiler
NeoBundle 'Shougo/vimfiler'

"vimデフォルトのエクスプローラをvimfilerで置き換える
let g:vimfiler_as_default_explorer = 1
""セーフモードを無効にした状態で起動する
let g:vimfiler_safe_mode_by_default = 0
"自動でカレントディレクトリを変更する
let g:vimfiler_enable_auto_cd = 1

"キーマッピング
nnoremap <silent> <F1> :<C-u>VimFilerExplorer<CR>
""現在開いているバッファをIDE風に開く
nmap <silent> <F2> :<C-u>VimFilerBufferDir -buffer-name='explorer' -split -simple -toggle -winwidth=35 -no-quit -columns=g:vimfiler_explorer_columns<CR>

"デフォルトタブオープン
"let g:vimfiler_edit_action = 'tabopen'
