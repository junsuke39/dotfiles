NeoBundle 'jnurmine/Zenburn'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'nanotech/jellybeans.vim'
NeoBundle 'w0ng/vim-hybrid'

colorscheme jellybeans
