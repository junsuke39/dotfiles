"unite.vim
NeoBundle 'Shougo/unite.vim'
NeoBundle 'tsukkee/unite-help'
NeoBundle 'tsukkee/unite-tag'
NeoBundle 'ujihisa/unite-colorscheme'
NeoBundle 'thinca/vim-unite-history'
NeoBundle 'Shougo/unite-outline'

"{{{ 基本設定
nnoremap [unite] <Nop>
nmap <Space> [unite]
let g:unite_split_rule = 'rightbelow'
"インサートモードで開始
let g:unite_enable_start_insert = 1
"最近開いたファイル履歴の保存数:new
let g:unite_source_file_mru_limit = 100
"file_mruの表示フォーマットを指定。空にすると表示スピードが高速化される
let g:unite_source_file_mru_filename_format = ''
"ヤンクされたテキストの履歴を候補とする。
let g:unite_source_history_yank_enable = 1
"}}}

"{{{ uniteを開いている間のキーマッピング
autocmd FileType unite call s:unite_my_settings()
function! s:unite_my_settings()"{{{
  "ESCでuniteを終了
  nmap <buffer> <ESC> <Plug>(unite_exit)
 "入力モードのときjjでノーマルモードに移動
  imap <buffer> jj <Plug>(unite_insert_leave)
  "入力モードのときctrl+wでバックスラッシュも削除
  imap <buffer> <C-w> <Plug>(unite_delete_backward_path)
  "ctrl+jで縦に分割して開く
  nnoremap <silent> <buffer> <expr> <C-j> unite#do_action('split')
  inoremap <silent> <buffer> <expr> <C-j> unite#do_action('split')
  "ctrl+kで横に分割して開く
  nnoremap <silent> <buffer> <expr> <C-k> unite#do_action('vsplit')
  inoremap <silent> <buffer> <expr> <C-k> unite#do_action('vsplit')
  "ctrl+oでその場所に開く
  nnoremap <silent> <buffer> <expr> <C-o> unite#do_action('open')
  inoremap <silent> <buffer> <expr> <C-o> unite#do_action('open')
endfunction"}}}
"}}}

"{{{ 様々なショートカット
call unite#custom#substitute('files', '\$\w\+', '\=eval(submatch(0))', 200)
call unite#custom#substitute('files', '^@@', '\=fnamemodify(expand("#"), ":p:h")."/"', 2)
call unite#custom#substitute('files', '^@', '\=getcwd()."/*"', 1)
call unite#custom#substitute('files', '^;r', '\=$VIMRUNTIME."/"')
call unite#custom#substitute('files', '^\~', escape($HOME, '\'), -2)
call unite#custom#substitute('files', '\\\@<! ', '\\ ', -20)
call unite#custom#substitute('files', '\\ \@!', '/', -30)
"}}}

call unite#custom_filters('buffer,buffer_tab', ['matcher_default', 'sorter_word'])

"{{{ キーマッピング
"inoremap <silent> <F1> <Nop>
nnoremap <silent> <F2> :<C-u>UniteResume<CR>
nnoremap <silent> <F3> :<C-u>UniteResume grep<CR>

nnoremap <silent> [unite]<Space> :<C-u>Unite buffer -buffer-name=buffers -no-split<CR>
"nnoremap <silent> [unite]n :<C-u>Unite buffer_tab -buffer-name=buffers -immediately<CR>
nnoremap <silent> [unite]. :<C-u>Unite -buffer-name=files -profile-name=files file -no-split<CR>
nnoremap <silent> [unite], :<C-u>Unite file_rec/async -no-split<CR>
nnoremap <silent> [unite]~ :<C-u>Unite -buffer-name=files -profile-name=files file -input=~ -start-insert -no-split<CR>
nnoremap <silent> [unite]h :<C-u>Unite file_mru -no-split<CR>
nnoremap <silent> [unite]b :<C-u>Unite bookmark -no-split<CR>
nnoremap <silent> [unite]c :<C-u>UniteWithBufferDir -buffer-name=files file -no-split<CR>
nnoremap <silent> [unite]o :<C-u>Unite outline<CR>

nnoremap <silent> [unite]/ :<C-u>Unite history/search<CR>
nnoremap <silent> [unite]: :<C-u>Unite history/command<CR>
nnoremap <silent> [unite]y :<C-u>Unite history/yank<CR>
nnoremap <silent> [unite]" :<C-u>Unite -buffer-name=register register<CR>

nnoremap <silent> [unite]a :<C-u>Unite source<CR>
nnoremap <silent> [unite]z :<C-u>Unite resume<CR>
nnoremap <silent> [unite]? :<C-u>Unite -start-insert -no-quit help<CR>
nnoremap <silent> [unite]$ :<C-u>Unite command<CR>

nnoremap <silent> [unite]# :<C-u>Unite colorscheme -auto-preview<CR>
nnoremap <silent> [unite]+ :<C-u>UniteBookmarkAdd<CR>

nnoremap <silent> [unite]s :<C-u>Unite -start-insert -buffer-name=files file_rec/async<CR>
nnoremap <silent> [unite]g :<C-u>Unite -buffer-name=grep grep<CR>
nnoremap <silent> [unite]f :<C-u>Unite -buffer-name=find find<CR>

"nnoremap <silent> [unite]w :<C-u>Unite window:no-current<CR>
"nnoremap <silent> [unite]t :<C-u>Unite tab:no-current<CR>
"nnoremap <silent> [unite]j :<C-u>Unite jump<CR>
"nnoremap <silent> [unite]d :<C-u>Unite change<CR>
"nnoremap <silent> [unite]l :<C-u>Unite -start-insert -no-quit line<CR>
"nnoremap <silent> [unite]@ :<C-u>Unite tag<CR>
"}}}

"{{{ rails
nnoremap <silent> [unite]rs :<C-u>Unite file_rec/async:app/assets/stylesheets/  -no-split<CR>
nnoremap <silent> [unite]rj :<C-u>Unite file_rec/async:app/assets/javascripts/  -no-split<CR>
nnoremap <silent> [unite]rc :<C-u>Unite file_rec/async:app/controllers/  -no-split<CR>
nnoremap <silent> [unite]rh :<C-u>Unite file_rec/async:app/helpers/  -no-split<CR>
nnoremap <silent> [unite]rm :<C-u>Unite file_rec/async:app/models/  -no-split<CR>
nnoremap <silent> [unite]rv :<C-u>Unite file_rec/async:app/views/  -no-split<CR>
nnoremap <silent> [unite]r, :<C-u>Unite file_rec/async:config/  -no-split<CR>
nnoremap <silent> [unite]rd :<C-u>Unite file_rec/async:db/  -no-split<CR>
nnoremap <silent> [unite]rl :<C-u>Unite file_rec/async:lib/  -no-split<CR>
nnoremap <silent> [unite]rt :<C-u>Unite file_rec/async:spec/  -no-split<CR>

"nnoremap <silent> [unite]rfc :<C-u>Unite file file/new -input=app/controllers/ <CR>
"nnoremap <silent> [unite]rfm :<C-u>Unite file file/new -input=app/models/ <CR>
"nnoremap <silent> [unite]rfv :<C-u>Unite file file/new -input=app/views/ <CR>
"nnoremap <silent> [unite]rfh :<C-u>Unite file file/new -input=app/helpers/ <CR>
"nnoremap <silent> [unite]rfs :<C-u>Unite file file/new -input=app/assets/stylesheets/ <CR>
"nnoremap <silent> [unite]rfj :<C-u>Unite file file/new -input=app/assets/javascripts/ <CR>
"nnoremap <silent> [unite]rfo :<C-u>Unite file file/new -input=config/ <CR>
"nnoremap <silent> [unite]rfl :<C-u>Unite file file/new -input=lib/ <CR>
"nnoremap <silent> [unite]rfr :<C-u>Unite file file/new -input=spec/ <CR>
"}}}

