"http://hail2u.net/blog/software/vim-wildfire.html
NeoBundle 'gcmt/wildfire.vim'
let g:wildfire_water_map = '<S-Enter>'
let g:wildfire_objects = ["i'", "a'", 'i"', 'a"', "i)", "i]", "i}", "ip", "it"]

