"{{{ SYSTEM
set nocompatible
set runtimepath+=~/.vim
"クリップボードをWindowsと連携
set clipboard=unnamed
"マウスを有効に
set mouse=a
"変更中のファイルでも、保存しないで他のファイルを表示
set hidden
"スワップファイルを作成しない
set noswapfile
"スワップファイル用のディレクトリ
set directory=$HOME/.vimbackup
"バックアップしない
set nobackup
"バックアップファイルを作るディレクトリ
set backupdir=$HOME/.vimbackup
"アンドゥファイルを作らない
set noundofile
"ファイル保存ダイアログの初期ディレクトリをバッファファイル位置に設定
set browsedir=buffer
"ビープ音消す
set visualbell
"}}}

"{{{ APPEARANCE
"シンタックスハイライト
syntax enable
"行番号表示
set number
"カーソルのある行をハイライトさせる
set cursorline
"カーソルを行頭、行末で止まらないようにする
set whichwrap=b,s,h,l,<,>,[,]
"""不可視文字
set list
set listchars=tab:»-,trail:_,eol:↲,extends:»,precedes:«,nbsp:%
"ウィンドウの右端で文字を折り返さない
set nowrap
set textwidth=0
autocmd FileType text setlocal textwidth=0
"折り返されている行の頭に++++を表示
set showbreak=++++
"上下最低２行を残してウィンドウをスクロールさせる
set scrolloff=5
"左右最低３文字を残してウィンドウをスクロールさせる
set sidescrolloff=3
"ステータスライン表示
set laststatus=2
"ステータスラインにファイルのパスと現在の文字コード等を出す
set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P
"★■などを正常に表示
set ambiwidth=double
"}}}

"{{{ SEARCH
"検索時に大文字小文字区別
set ignorecase
"検索時に大文字を含んでいたら大/小を区別
set smartcase
"インクリメンタルサーチを行う
set incsearch
"検索をファイルの先頭へループしない
"set nowrapscan
set grepprg=ack\ --nogroup\ --column\ $*
set grepformat=%f:%l:%c:%m
"}}}

"{{{ INDENT
"新しい行のインデントを現在行と同じにする
set autoindent
"新しい行を作ったときに高度な自動インデントを行う
set smartindent
"タブの代わりに空白文字を挿入する
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
"シフト移動幅
"set shiftwidth=4
"行頭の余白内で Tab を打ち込むと、'shiftwidth' の数だけインデントする。
set smarttab
"ファイル内の <Tab> が対応する空白の数
"set tabstop=4
"}}}

"{{{ ETC
"閉じ括弧が入力されたとき、対応する括弧を表示する
set noshowmatch
"モードライン
set modeline
"}}}
