" OpenCurrBufAsNewTab
function! OpenCurrBufAsNewTab()
    let f = expand("%:p")
    execute ":tabnew ".substitute(f, " ", "\\\\ ", "g")
endfunction

" Anywhere SID.
function! s:SID_PREFIX()
  return matchstr(expand('<sfile>'), '<SNR>\d\+_\zeSID_PREFIX$')
endfunction

" Set tabline.
function! s:my_tabline()  "{{{
  let s = ''
  for i in range(1, tabpagenr('$'))
    let bufnrs = tabpagebuflist(i)
    let bufnr = bufnrs[tabpagewinnr(i) - 1]  " first window, first appears
    let no = i  " display 0-origin tabpagenr.
    let mod = getbufvar(bufnr, '&modified') ? '!' : ' '
    let title = fnamemodify(bufname(bufnr), ':t')
    let title = '[' . title . ']'
    let s .= '%'.i.'T'
    let s .= '%#' . (i == tabpagenr() ? 'TabLineSel' : 'TabLine') . '#'
    let s .= no . ':' . title
    let s .= mod
    let s .= '%#TabLineFill# '
  endfor
  let s .= '%#TabLineFill#%T%=%#TabLine#'
  return s
endfunction "}}}
let &tabline = '%!'. s:SID_PREFIX() . 'my_tabline()'

set showtabline=2 " 常にタブラインを表示

" http://hail2u.net/blog/software/vim-move-tabpage.html
" Move tabpage
function! s:MoveTabpage(num)
  if type(a:num) != type(0)
    return
  endif
  let pos = tabpagenr() - 1 + a:num
  let tabcount = tabpagenr("$")
  if pos < 0
    let pos = tabcount - 1
  elseif pos >= tabcount 
    let pos = 0
  endif
  execute "tabmove " . pos
endfunction
" TabMove: Move tabpage with reltive number
command! -nargs=1 TabMove :call <SID>MoveTabpage(<f-args>)

""" KEY MAPPING

"タブ
nnoremap <silent> <C-t> <Nop>
nnoremap <silent> <C-t><C-t> :tablast <bar> call OpenCurrBufAsNewTab()<CR>
nnoremap <silent> <C-t>n :<C-u>tabedit<CR>
nnoremap <silent> <C-t>c :<C-u>tabclose<CR>
nnoremap <silent> <C-t><Enter> :<C-u>tabclose<CR>
nnoremap <silent> <C-t>o :<C-u>tabonly<CR>
nnoremap <silent> <C-t>l :<C-u>tabnext<CR>
nnoremap <silent> <C-t>h :<C-u>tabprev<CR>

"タブ移動
nnoremap <silent> <C-j> :<C-u>tabnext<CR>
nnoremap <silent> <C-k> :<C-u>tabprev<CR>
nnoremap <C-l> :call <SID>MoveTabpage(1)<Return>
nnoremap <C-h>  :call <SID>MoveTabpage(-1)<Return>

