"カーソル移動
noremap <silent> <S-j> 3j
noremap <silent> <S-k> 3k
noremap <silent> <S-l> $
noremap <silent> <S-h> ^
vnoremap <silent> <S-l> $h

imap <C-j> <Down>
imap <C-k> <Up>
imap <C-h> <Left>
imap <C-l> <Right>

"バッファ移動
"nnoremap <silent> <C-l> :<C-u>bn<CR>
"nnoremap <silent> <C-h> :<C-u>bp<CR> 

"ウィンドウ移動
nnoremap <silent> <S-Up> <C-w>k
nnoremap <silent> <S-Down> <C-w>j
nnoremap <silent> <S-Right> <C-w>l
nnoremap <silent> <S-Left> <C-w>h

"バッファ削除
nnoremap <silent> <C-q> :<C-u>Bclose<CR>

"行連結
noremap <silent> zj <S-j>
noremap <silent> z<S-j> g<S-j>

"vv で行末まで選択
vnoremap v ^$h

"選択範囲のインデントを連続して変更
vnoremap < <gv
vnoremap > >gv

"その他
nmap <C-s> :w<CR>
imap <C-s> <Esc>:w<CR>

"x キー削除でデフォルトレジスタに入れない
nnoremap x "_x
vnoremap x "_x

"スクリーン再描画でハイライトもクリア
noremap <silent> <F5> :<C-u>noh<CR>:<C-u>redraw<CR>

"自動閉じ
"http://d.hatena.ne.jp/spiritloose/20061113/1163401194
""inoremap { {}<LEFT>
""inoremap [ []<LEFT>
""inoremap ( ()<LEFT>
""inoremap " ""<LEFT>
""inoremap ' ''<LEFT>
""vnoremap { "zdi{<C-R>z}<ESC>
""vnoremap [ "zdi[<C-R>z]<ESC>
""vnoremap ( "zdi(<C-R>z)<ESC>
""vnoremap " "zdi"<C-R>z"<ESC>
""vnoremap ' "zdi'<C-R>z'<ESC>

"コマンドラインで %% => %:h
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

"visualmodeから*
vnoremap * "zy:let @/ = @z<CR>n

"行末までコピー
nnoremap Y y$

