PS1=$'%n@%m:%~\n$ '
RPS1=$'%D'

## options
setopt BASH_AUTO_LIST
setopt LIST_AMBIGUOUS

# (d) is default on
# ------------------------------
# General Settings
# ------------------------------
export EDITOR=vim        # エディタをvimに設定
export LANG=ja_JP.UTF-8  # 文字コードをUTF-8に設定
export KCODE=u           # KCODEにUTF-8を設定
export AUTOFEATURE=true  # autotestでfeatureを動かす

#bindkey -e               # キーバインドをemacsモードに設定
bindkey -v              # キーバインドをviモードに設定

setopt no_beep           # ビープ音を鳴らさないようにする
setopt nolistbeep
setopt auto_cd           # ディレクトリ名の入力のみで移動する 
setopt auto_pushd        # cd時にディレクトリスタックにpushdする
setopt pushd_ignore_dups # auto_pushdで重複するディレクトリは記録しないようにする
#setopt correct           # コマンドのスペルを訂正する
setopt magic_equal_subst # =以降も補完する(--prefix=/usrなど)
setopt prompt_subst      # プロンプト定義内で変数置換やコマンド置換を扱う
setopt notify            # バックグラウンドジョブの状態変化を即時報告する
setopt equals            # =commandを`which command`と同じ処理にする

### Complement ###
autoload -U compinit; compinit # 補完機能を有効にする
setopt auto_list               # 補完候補を一覧で表示する(d)
setopt auto_menu               # 補完キー連打で補完候補を順に表示する(d)
setopt list_packed             # 補完候補をできるだけ詰めて表示する
setopt list_types              # 補完候補にファイルの種類も表示する
bindkey "^[[Z" reverse-menu-complete  # Shift-Tabで補完候補を逆順する("\e[Z"でも動作する)
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' # 補完時に大文字小文字を区別しない
zstyle ':completion:*' menu select=1 #候補選択
#setopt complete_aliases # aliasを補完候補に含める(cd-bookmarkとの相性が悪いのでコメントアウト)
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin # コマンドにsudoを付けてもきちんと補完出来るようにする

### Glob ###
setopt extended_glob # グロブ機能を拡張する
unsetopt caseglob    # ファイルグロブで大文字小文字を区別しない

### History ###
HISTFILE=~/.zsh_history   # ヒストリを保存するファイル
HISTSIZE=10000            # メモリに保存されるヒストリの件数
SAVEHIST=10000            # 保存されるヒストリの件数
setopt bang_hist          # !を使ったヒストリ展開を行う(d)
setopt extended_history   # ヒストリに実行時間も保存する
setopt hist_ignore_dups   # 直前と同じコマンドはヒストリに追加しない
setopt share_history      # 他のシェルのヒストリをリアルタイムで共有する
setopt hist_reduce_blanks # 余分なスペースを削除してヒストリに保存する
setopt hist_ignore_all_dups # 重複するコマンドが記憶されるとき、古い方を削除する
setopt hist_save_no_dups  # 重複するコマンドが保存されるとき、古い方を削除する
setopt share_history      # 履歴を複数端末間で共有する
setopt hist_no_store      # historyコマンドは履歴に登録しない
setopt hist_ignore_space  # コマンドがスペースで始まる場合、コマンド履歴に追加しない

# マッチしたコマンドのヒストリを表示できるようにする
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end
bindkey "^R" history-incremental-search-backward

# ------------------------------
# Look And Feel Settings
# ------------------------------
### Ls Color ###
# 色の設定
export LSCOLORS=Exfxcxdxbxegedabagacad
# 補完時の色の設定
export LS_COLORS='di=01;34:ln=01;35:so=01;32:ex=01;31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
# ZLS_COLORSとは？
export ZLS_COLORS=$LS_COLORS
# lsコマンド時、自動で色がつく(ls -Gのようなもの？)
export CLICOLOR=true
# 補完候補に色を付ける
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

### Prompt ###
autoload -U colors; colors
PROMPT="%{${fg[cyan]}%}%n%# %{${reset_color}%}"    # 通常のプロンプト
PROMPT2="%{${fg[cyan]}%}%_> %{${reset_color}%}"  # セカンダリのプロンプト(コマンドが2行以上の時に表示される)
RPROMPT="%{${fg[green]}%}[%~]%{${reset_color}%}"  # 右側のプロンプト
SPROMPT="%{${fg[cyan]}%}%r is correct? [Yes, No, Abort, Edit]:%{${reset_color}%}"  # スペル訂正用プロンプト

### Title (user@hostname) ###
case "${TERM}" in
kterm*|xterm*|)
  precmd() {
    echo -ne "\033]0;${USER}@${HOST%%.*}\007"
  }
  ;;
esac

### Aliases ###

alias df="df -h"
alias cl="clear"
alias up="cd .."
alias history-all="history -E 1"

# vimで<C-q><C-s>を使用可能にする
stty -ixon

#viキーバインドでコマンドラインスタック
bindkey -a 'q' push-line

# cool-peco
source /usr/local/src/cool-peco/cool-peco
alias s=cool-peco-ssh
alias h=cool-peco-history

# インクルード
case "${OSTYPE}" in
freebsd*|darwin*)
  [ -f ~/.zshrc.mac ] && source ~/.zshrc.mac
  ;;
linux*)
  [ -f ~/.zshrc.linux ] && source ~/.zshrc.linux
  ;;
esac
[ -f ~/.zshrc.mine ] && source ~/.zshrc.mine
