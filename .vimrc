" vim: fdm=marker

"""PLUGINS
"neobundle before
if has('vim_starting')
  set nocompatible               " Be iMproved
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif
call neobundle#rc(expand('~/.vim/bundle/'))
NeoBundleFetch 'Shougo/neobundle.vim'
"basic
source ~/.vim/rc/inc/plugin/color-scheme.vim
source ~/.vim/rc/inc/plugin/text-object.vim
source ~/.vim/rc/inc/plugin/vim-smartinput.vim
source ~/.vim/rc/inc/plugin/accelerated-smooth-scroll.vim
source ~/.vim/rc/inc/plugin/vim-easymotion.vim
source ~/.vim/rc/inc/plugin/vimproc.vim
source ~/.vim/rc/inc/plugin/vimshell.vim
"source ~/.vim/rc/inc/plugin/nerdtree.vim
source ~/.vim/rc/inc/plugin/vimfiler.vim
"unite.vim
source ~/.vim/rc/inc/plugin/unite.vim
"neocomplcache
"source ~/.vim/rc/inc/plugin/neocomplcache.vim
source ~/.vim/rc/inc/plugin/neocomplete.vim
"etc
source ~/.vim/rc/inc/plugin/emmet-vim.vim
source ~/.vim/rc/inc/plugin/vim-endwise.vim
source ~/.vim/rc/inc/plugin/lightline.vim
source ~/.vim/rc/inc/plugin/tcomment_vim.vim
"source ~/.vim/rc/inc/plugin/wildfire.vim
"source ~/.vim/rc/inc/plugin/zencoding.vim
"NeoBundle 'tpope/vim-rvm'
"NeoBundle 'vim-ruby/vim-ruby'
"NeoBundle 'tpope/vim-rails'
"NeoBundle 'ujihisa/unite-font'
"NeoBundle 'rson/vim-conque'
"NeoBundle 'rails.vim'                           " vim-scripts repos
"NeoBundle 'git://git.wincent.com/command-t.git' " non github repos

"neobundle after
filetype plugin indent on
NeoBundleCheck

"""BASIC
source ~/.vim/rc/inc/basic/option.vim
source ~/.vim/rc/inc/basic/bclose.vim
source ~/.vim/rc/inc/basic/key-map.vim
"source ~/.vim/rc/inc/basic/statusline-color.vim
source ~/.vim/rc/inc/basic/cusor_by_mode.vim
source ~/.vim/rc/inc/basic/tab_customize.vim

"""UTIL
"source ~/.vim/rc/inc/util/replace_word.vim
source ~/.vim/rc/inc/util/erb-tag_support.vim
source ~/.vim/rc/inc/util/kobito.vim
"source ~/.vim/rc/inc/util/paste_from_yank_register.vim
"source ~/.vim/rc/inc/util/close_bracket.vim
"source ~/.vim/rc/inc/util/tabline.vim
"source ~/.vim/rc/inc/util/indent_empty_line.vim

